package com.example.bmi_calculator;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class HeightBottomDialogMenu extends BottomSheetDialogFragment {
    private HeightBottomMenuListener mListener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.bottom_height_layout, container, false);

        Button centimetreLabelButton = view.findViewById(R.id.select_centimetres_unit);
        Button metreLabelButton = view.findViewById(R.id.select_metres_unit);
        Button feetsLabelButton = view.findViewById(R.id.select_feets_unit);
        Button inchesLabelButton = view.findViewById(R.id.select_inches_unit);

        centimetreLabelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onHeightButtonClicked("Centimetres");
                dismiss();
            }
        });
        metreLabelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onHeightButtonClicked("Metres");
                dismiss();
            }
        });
        feetsLabelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onHeightButtonClicked("Feets");
                dismiss();
            }
        });
        inchesLabelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onHeightButtonClicked("Inches");
                dismiss();
            }
        });
        return view;
    }

    public interface HeightBottomMenuListener {
        void onHeightButtonClicked(String unit);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            mListener = (HeightBottomMenuListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement HeightBottomMenuListener");
        }
    }
}
