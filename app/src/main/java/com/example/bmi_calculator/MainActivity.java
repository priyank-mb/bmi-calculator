package com.example.bmi_calculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements WeightBottomDialogMenu.WeightBottomMenuListener, HeightBottomDialogMenu.HeightBottomMenuListener {
    private static final String POUND_UNIT = "Pound";
    private static final String CENTIMETRES_UNIT = "Centimetre";
    private static final String INCHES_UNIT = "Inches";
    private static final String FEETS_UNIT = "Feets";
    private static final String ZERO = "0";
    private static final String DECIMAL_POINT = ".";

    private TextView mEditWeight;
    private TextView mEditHeight;
    private Button mButtonCalc;
    private Button mSelectWeightButton;
    private Button mSelectHeightButton;
    private TextView mWeightUnit;
    private TextView mHeightUnit;
    private TextView mResultDisplay;
    private TextView mResultCategory;
    private TextView focusedEditText;
    private GridLayout keyboardLayout;
    private ConstraintLayout resultLayout;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditWeight = findViewById(R.id.weight_entry);
        mEditHeight = findViewById(R.id.height_entry);
        mButtonCalc = findViewById(R.id.calc_bmi);
        mSelectWeightButton = findViewById(R.id.weight_label);
        mSelectHeightButton = findViewById(R.id.height_label);
        mWeightUnit = findViewById(R.id.weight_unit);
        mHeightUnit = findViewById(R.id.height_unit);
        keyboardLayout = findViewById(R.id.keyboard_layout);
        resultLayout = findViewById(R.id.result_layout);
        mResultDisplay = findViewById(R.id.result_text);
        mResultCategory = findViewById(R.id.result_category);

        mEditWeight.setText(R.string.default_weight);
        mEditHeight.setText(R.string.defult_height);

        focusedEditText = mEditWeight;

        mSelectWeightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeightBottomDialogMenu weightBottomDialogMenu = new WeightBottomDialogMenu();
                weightBottomDialogMenu.show(getSupportFragmentManager(), "bottom_weight_layout");
            }
        });

        mSelectHeightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HeightBottomDialogMenu heightBottomDialogMenu = new HeightBottomDialogMenu();
                heightBottomDialogMenu.show(getSupportFragmentManager(), "bottom_height_layout");
            }
        });

    }
    public void editableListener(View v) {
        focusedEditText = (TextView) v;
        focusedEditText.setText(R.string.zero);
        keyboardLayout.setVisibility(View.VISIBLE);
        resultLayout.setVisibility(View.INVISIBLE);
    }

    public void showResult(View v) {
        String height = mEditHeight.getText().toString();
        String weight = mEditWeight.getText().toString();

        if (height.length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter a valid height!", Toast.LENGTH_SHORT);
        } else if (weight.length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter a valid weight!", Toast.LENGTH_SHORT);
        } else {
            String weightUnit = mWeightUnit.getText().toString();
            String heightUnit = mHeightUnit.getText().toString();
            Double heightValue = Double.parseDouble(height);
            Double weightValue = Double.parseDouble(weight);
            if (weightUnit.equals(POUND_UNIT)) {
                weightValue = weightValue*0.4536;
            }
            if (heightUnit.equals(CENTIMETRES_UNIT)) {
                heightValue = heightValue / 100;
            } else if (heightUnit.equals(INCHES_UNIT)) {
                heightValue = heightValue * 0.0254;
            } else if (heightUnit.equals(FEETS_UNIT)) {
                heightValue = heightValue * 0.0254 * 12;
            }
            Double result = weightValue / (heightValue*heightValue);
            String resultString = String.format("%.2f", result);

            if (result < 18.5) {
                mResultCategory.setText(R.string.underweight);
                keyboardLayout.setVisibility(View.INVISIBLE);
                resultLayout.setVisibility(View.VISIBLE);
                mResultDisplay.setText(resultString);
            } else if (result < 25) {
                mResultCategory.setText(R.string.normal);
                keyboardLayout.setVisibility(View.INVISIBLE);
                resultLayout.setVisibility(View.VISIBLE);
                mResultDisplay.setText(resultString);
            } else if (result <= 40) {
                mResultCategory.setText(R.string.overweight);
                keyboardLayout.setVisibility(View.INVISIBLE);
                resultLayout.setVisibility(View.VISIBLE);
                mResultDisplay.setText(resultString);
            } else {
                Toast.makeText(getApplicationContext(), "This BMI does't look right. Make sure height and weight entered is correct.", Toast.LENGTH_SHORT);
            }
        }
    }

    public void listenButton(View v) {
        Button b = (Button)v;
        String bString = b.getText().toString();
        if(focusedEditText != null) {
            String viewValue = focusedEditText.getText().toString();
            if (viewValue.length() <= 1 && viewValue.equals(ZERO)) {
                focusedEditText.setText(bString);
            } else if (!(viewValue.contains(DECIMAL_POINT) && bString.equals(DECIMAL_POINT) && viewValue.length() <= 5 )){
                viewValue += bString;
                double value = Double.parseDouble(viewValue);
                if (value < R.integer.max_number ) {
                    focusedEditText.append(bString);
                }
            }
        }
    }

    public void clearDigit(View v) {
        String str = focusedEditText.getText().toString();

        if (str != null && str.length() > 1) {
            str = str.substring(0, str.length() - 1);
            focusedEditText.setText(str);
        } else {
            focusedEditText.setText(R.string.zero);
        }
    }

    @Override
    public void onWeightButtonClicked(String unit) {
        mWeightUnit.setText(unit);
    }

    @Override
    public void onHeightButtonClicked(String unit) {
        mHeightUnit.setText(unit);
    }
}
