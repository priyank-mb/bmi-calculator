package com.example.bmi_calculator;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class WeightBottomDialogMenu extends BottomSheetDialogFragment {
    private WeightBottomMenuListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_weight_layout, container, false);

        Button kilogramLabelButton = view.findViewById(R.id.select_kilo_unit);
        Button poundLabelButton = view.findViewById(R.id.select_pound_unit);

        kilogramLabelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onWeightButtonClicked("Kilogram");
                dismiss();
            }
        });
        poundLabelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onWeightButtonClicked("Pound");
                dismiss();
            }
        });
        return view;
    }

    public interface WeightBottomMenuListener {
        void onWeightButtonClicked(String unit);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            mListener = (WeightBottomMenuListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement weightBottomMenuListener");
        }
    }
}
